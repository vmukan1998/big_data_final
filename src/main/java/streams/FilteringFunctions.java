package streams;

public class FilteringFunctions {

    public static boolean isInCentre(String lat, String lng, double radius) {

        return Math.sqrt(Math.pow(49.842547 - Float.valueOf(lat), 2)
                + Math.pow(24.026544 - Float.valueOf(lng), 2)) <= radius;
    }
}
