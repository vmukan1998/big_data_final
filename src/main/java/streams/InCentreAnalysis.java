package streams;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;

import java.util.Properties;

public class InCentreAnalysis implements Runnable {

    private Properties streamProps;
    private double radius;

    public InCentreAnalysis(double radius) {
        this.radius = radius;
        streamProps = new Properties();

        streamProps.put(StreamsConfig.APPLICATION_ID_CONFIG, "in-centre-stream");
        streamProps.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        streamProps.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        streamProps.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, GenericAvroSerde.class);
        streamProps.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, GenericAvroSerde.class);
    }

    @Override
    public void run() {

        KStreamBuilder streamBuilder = new KStreamBuilder();
        KStream<GenericRecord, GenericRecord> messages = streamBuilder.stream("cars-position");

        messages
                .filter((k, v)->
                        FilteringFunctions.isInCentre(v.get("latitude").toString(),v.get("longitude").toString(), radius))
                .to("in-centre");


        KafkaStreams streams = new KafkaStreams(streamBuilder, streamProps);
        streams.start();
    }
}
