import Data.Producer;
import streams.InCentreAnalysis;
import streams.StatusAnalysis;

public class Main {
    public static void main(String[] args) {

        Thread producerThread = new Thread(new Producer());
        Thread statusAnalysis = new Thread(new StatusAnalysis());
        Thread carsInCentreAnalysis = new Thread(new InCentreAnalysis(0.01));//0.0015));

        producerThread.start();
        //statusAnalysis.start();
        carsInCentreAnalysis.start();



    }
}