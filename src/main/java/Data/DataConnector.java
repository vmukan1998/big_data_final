package Data;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DataConnector {

    private Properties props;

    private String path;

    public DataConnector(Properties props, String path) {
        this.props = props;
        this.path = path;
    }

    public List<Cars> read(int second) {

        List<Cars> data = new ArrayList<>();
        String selectQuery = "SELECT time, car_number, group_number, latitude, longitude, status FROM taxi_cars WHERE time = ?";

        try (Connection connection = DriverManager.getConnection(path, props);
             PreparedStatement statement = connection.prepareStatement(selectQuery);
        ) {
            statement.setInt(1, second);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                data.add(new Cars(
                        resultSet.getInt("time"),
                        resultSet.getString("car_number"),
                        resultSet.getInt("group_number"),
                        resultSet.getFloat("latitude"),
                        resultSet.getFloat("longitude"),
                        resultSet.getString("status").equals("1")
                ));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return data;
    }
}
