package streams;

import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class StatusAnalysis implements Runnable{
    private Properties streamProps;

    public StatusAnalysis() {
        streamProps = new Properties();

        streamProps.put(StreamsConfig.APPLICATION_ID_CONFIG, "free-cars-stream");
        streamProps.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        streamProps.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        streamProps.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, GenericAvroSerde.class);
        streamProps.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, GenericAvroSerde.class);
    }


    @Override
    public void run() {

        KStreamBuilder streamBuilder = new KStreamBuilder();
        KStream<GenericRecord, GenericRecord> messages = streamBuilder.stream("cars-position");

//        KTable<Windowed<Boolean>, Long> countFreeCars = messages
//                .map(v -> v.get("status").toString())
//                .count(SessionWindows.with(TimeUnit.HOURS.toSeconds(3)), "views");
////                .mapValues(i -> i.get("status").toString())
//                .selectKey((key, val) -> val)
//                .groupByKey().count();
//        countFreeCars.toStream().print();


        KafkaStreams streams = new KafkaStreams(streamBuilder, streamProps);
        streams.start();

        //KTable<GenericRecord, GenericRecord> countFreeCars;
        //filter((key, value)-> value.get("status").toString().equals("false"))
               // .map((key, value)-> KeyValue.pair(value.get("car_number").toString(), value.get("status").toString());


//                .mapValues(i -> i.get("status").toString())
//                .selectKey((key, val) -> val)
//                .groupByKey(Serdes.String(), Serdes.String())


       // countFreeCars.toStream().print(Serdes.String(), Serdes.Long());
                // countFreeCars.toStream().to(Serdes.String(), Serdes.Long(), "cars-status");


            
    }
}
