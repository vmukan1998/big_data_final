package Data;

public class Cars {

    private int time;
    private String car_number;
    private int group_number;
    private float latitude;
    private float longitude;
    private boolean status;

    public Cars(int time, String car_number, int group_number, float latitude, float longitude, boolean status) {
        this.time = time;
        this.car_number = car_number;
        this.group_number = group_number;
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Data.Cars{" +
                "time=" + time +
                ", car_number='" + car_number + '\'' +
                ", group_number=" + group_number +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", status=" + status +
                '}';
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getCar_number() {
        return car_number;
    }

    public void setCar_number(String car_number) {
        this.car_number = car_number;
    }

    public int getGroup_number() {
        return group_number;
    }

    public void setGroup_number(int group_number) {
        this.group_number = group_number;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
