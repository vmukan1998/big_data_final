package Data;

import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Producer implements Runnable{

    private String topicName;
    private Properties kafkaProp;
    private DataConnector dataReader;

    public Producer() {
        Properties dbProp = new Properties();
        dbProp.setProperty("user", "root");
        dbProp.setProperty("password", "root");
        dbProp.setProperty("useSSL", "false");
        String dbPath = "jdbc:mysql://localhost/eleks";
        dataReader = new DataConnector(dbProp, dbPath);

        topicName = "cars-position";
        kafkaProp = new Properties();

        kafkaProp.put("bootstrap.servers", "localhost:9092");
        kafkaProp.put("key.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
        kafkaProp.put("value.serializer", "io.confluent.kafka.serializers.KafkaAvroSerializer");
        kafkaProp.put("schema.registry.url", "http://localhost:8081");
        kafkaProp.put("acks", "all");

    }
    @Override
    public void run() {
        int second = 0;

        while (!Thread.currentThread().isInterrupted()) {
            sendData(second);
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            second = second == 1799 ?  0 : second + 1;
        }

    }


    public void sendData(int second) {

        List<Cars> cars = dataReader.read(second);

        KafkaProducer<GenericRecord, GenericRecord> producer = new KafkaProducer<>(kafkaProp);

        Schema schema = null;

        File avroFile = new File("src/main/resources/avro/CarsSchema.avsc");

        try {
            schema = new Schema.Parser().parse(avroFile);
        } catch(IOException e) {
            e.printStackTrace();
        }

        ProducerRecord<GenericRecord, GenericRecord> producerRecord;
        GenericRecord genericRecord;

        for (Cars car : cars) {
            genericRecord = new GenericData.Record(schema);
            genericRecord.put("time", car.getTime());
            genericRecord.put("car_number", car.getCar_number());
            genericRecord.put("group_number", car.getGroup_number());
            genericRecord.put("latitude", car.getLatitude());
            genericRecord.put("longitude", car.getLongitude());
            genericRecord.put("status", car.isStatus());

            producerRecord = new ProducerRecord<>(topicName, null, genericRecord);
            producer.send(producerRecord);
        }
    }
}
